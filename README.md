## Instructions 

## How to run
  In order to run the app you need to follow below steps,

* Install project dependencies: `npm i`
* To start the application: `npm start`
* To run the tests: `npm test`

## Documentation

1. Load all the items from the provided product feed file (products.csv) in Grid List.

2. A page is having a search box with an auto-complete feature which will display all the products in a grid list below the search component.

3. The list will be updated on every key-press event.