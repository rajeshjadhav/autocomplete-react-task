import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render, configure } from 'enzyme';
import Enzyme from 'enzyme';
import { mountWithIntl, shallowWithIntl, loadTranslation } from 'enzyme-react-intl';
import renderer from 'react-test-renderer';
import Button from '@material-ui/core/Button';

import ReaderContainer from '../src/containers/csv/';

Enzyme.configure({ adapter: new Adapter() });

const wrapper = mountWithIntl(shallow(<ReaderContainer />).get(0));
const inst = wrapper.instance();

it('App renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('TextField renders without crashing', () => {    
  expect(wrapper.find('input[placeholder="Search title"]').exists()).toEqual(true);
});

it('TextField value set/change', () => {  
  const getInput = wrapper.find('input[placeholder="Search title"]');
  getInput.simulate('focus');
  getInput.simulate('change', { target: { value: 'Diesel AMETALINE Armband schwarz' } });
});

it('Products renders without crashing', () => {  
  const products = [
    {
      additional_image_link: "https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@22.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@21.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@20.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@19.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@18.jpg",
      gender: "female",
      gtin: "2001007926858",
      image_link: "https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@12.4.jpg",
      price: "39.95 EUR",
      sale_price: "39.95 EUR",
      title: "Weekday THURSDAY Jeans Slim Fit black"
    },
    {
      additional_image_link: "https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@22.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@21.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@20.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@19.jpg, https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@18.jpg",
      gender: "female",
      gtin: "2001007926858",
      image_link: "https://mosaic01.ztat.net/vgs/media/large/WE/B2/1N/00/HQ/11/WEB21N00H-Q11@12.4.jpg",
      price: "39.95 EUR",
      sale_price: "39.95 EUR",
      title: "Diesel black"
    }
  ];
  inst.state.rowsData = products;  
});
