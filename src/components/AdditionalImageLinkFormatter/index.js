import React from "react";
import Image from "../Image/";

const AdditionalImageLinkFormatter = ({ row }) => {
    let additionalImageLink = row.additional_image_link.split(",");
    return (      
        <Image row={additionalImageLink} lgClass={2}/>
    )
};
export default AdditionalImageLinkFormatter;