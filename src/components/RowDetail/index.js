import React from "react";
import AdditionalImageLinkFormatter from "../AdditionalImageLinkFormatter/";

const RowDetail = ({ row }) => {    
    return (        
        <AdditionalImageLinkFormatter row={row}/>
    )
};

export default RowDetail;