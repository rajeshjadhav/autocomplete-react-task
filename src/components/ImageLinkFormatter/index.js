import React from "react";
import Image from "../Image/";

const ImageLinkFormatter = ({ row }) => {

    return (
        <Image row={[row.image_link]} lgClass={2}/>
    )
};

export default ImageLinkFormatter;