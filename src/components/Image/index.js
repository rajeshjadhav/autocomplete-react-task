import React from "react";
import { Grid } from '@material-ui/core/';

const Image = ({ row, lgClass }) => {    
    let imageData =
        row.map((item, i) => {
            return (
                <Grid item lg={lgClass} key={i}>
                    <img 
                        src={'//upload.wikimedia.org/wikipedia/commons/1/18/React_Native_Logo.png'}
                        alt={item.title}
                        className="thumbImg"
                    />
                </Grid>
            )
        });
    return (
        <Grid item lg={12} className='gridThumbContaier'>
            {imageData}
        </Grid>
    )
}
export default Image;