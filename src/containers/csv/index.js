import "../../assets/css/style.css";
import { TextField, Paper, MenuItem, Grid as GridMaterial } from '@material-ui/core/';
import { withStyles } from '@material-ui/core/styles';
import Autosuggest from 'react-autosuggest';
import deburr from 'lodash/deburr';
import ImageLinkFormatter from "../../components/ImageLinkFormatter/";
import match from 'autosuggest-highlight/match';
import Papa from 'papaparse';
import parse from 'autosuggest-highlight/parse';
import Products from '../../products.csv';
import PropTypes from 'prop-types';
import React, { Component } from "react";
import RowDetail from "../../components/RowDetail/";
import {
    DataTypeProvider, IntegratedPaging, IntegratedSelection, RowDetailState,
    PagingState, SelectionState, TreeDataState,
} from '@devexpress/dx-react-grid';
import {
    Grid, PagingPanel,
    Table, TableRowDetail,
    TableHeaderRow, TableColumnResizing, TableSelection,
} from '@devexpress/dx-react-grid-material-ui';

const styles = theme => ({
    root: {
        height: 250,
        flexGrow: 1,
    },
    container: {
        position: 'relative',
        width: 700
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
        width: 700
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    divider: {
        height: theme.spacing.unit * 2,
    },
});

class ReaderContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageSize: 100,
            pageSizes: [100, 150, 200],
            columns: [
                { name: 'title', title: 'TITLE' },
                { name: 'gtin', title: 'GTIN' },
                { name: 'gender', title: 'GENDER' },
                { name: 'sale_price', title: 'SALE PRICE' },
                { name: 'price', title: 'PRICE' },
                { name: 'image_link', title: 'IMAGE LINK' },
            ],
            rows: [],
            rowsData: [],
            defaultColumnWidths: [
                { columnName: 'title', width: 350 },
                { columnName: 'gtin', width: 140 },
                { columnName: 'gender', width: 140 },
                { columnName: 'sale_price', width: 140 },
                { columnName: 'price', width: 140 },
                { columnName: 'image_link', width: 580 },
            ],
            customColumns1: ['image_link'],
            rowHeight: 200,
            expandedRowIds: [],
            suggestions: [],
            value: '',
            single: '',
            popper: '',
            selectedSuggestion: []
        };
    }

    componentDidMount() {
        Papa.parse(Products, {
            header: true,
            download: true,
            skipEmptyLines: true,
            complete: this.storeData
        });
    }

    changeExpandedDetails = (expandedRowIds) => {
        this.setState({ expandedRowIds });
    }

    storeData = (data) => {
        this.setState({ rowsData: data.data });
    }

    renderInputComponent = (inputProps) => {
        const { classes, inputRef = () => { }, ref, ...other } = inputProps;
        return (
            <TextField
                fullWidth
                InputProps={{
                    inputRef: node => {
                        ref(node);
                        inputRef(node);
                    },
                    classes: {
                        input: classes.input,
                    },
                }}
                {...other}
                className="autosuggest"
            />
        );
    }

    renderSuggestion = (suggestion, { query, isHighlighted }) => {
        const matches = match(suggestion.title, query);
        const parts = parse(suggestion.title, matches);

        return (
            <MenuItem selected={isHighlighted} component="div" lg={12}>
                <div>
                    {parts.map((part, index) => {

                        return part.highlight ? (
                            <span key={String(index)} style={{ fontWeight: 500 }}>
                                {part.text}
                            </span>
                        ) : (
                                <strong key={String(index)} style={{ fontWeight: 300 }}>
                                    {part.text}
                                </strong>
                            );
                    })}
                </div>
            </MenuItem>
        );
    }

    getSuggestions = (value) => {
        const { rowsData: suggestions } = this.state;
        const inputValue = deburr(value.trim()).toLowerCase();
        const inputLength = inputValue.length;
        let count = 0;

        return inputLength === 0
            ? []
            : suggestions.filter(suggestion => {
                const keep =
                    count < 10 && suggestion.title.slice(0, inputLength).toLowerCase() === inputValue;

                if (keep) {
                    count += 1;
                }

                return keep;
            });
    }

    getSuggestionValue = (suggestion) => {
        return suggestion.title;
    }

    handleSuggestionsFetchRequested = ({ value }) => {
        const foundSuggestions = this.getSuggestions(value);
        this.setState({
            suggestions: foundSuggestions,
        });
    };

    handleSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    handleChange = name => (event, { newValue }) => {
        this.setState({
            [name]: newValue,
        });
        if (newValue === '') {
            this.setState({
                selectedSuggestion: []
            });
        }
    };

    onSuggestionSelected = (event, value) => {
        this.setState({
            selectedSuggestion: [value.suggestion],
        });
    }

    render() {
        const { rowsData, rowHeight, columns, expandedRowIds, customColumns1, pageSize, pageSizes, defaultColumnWidths,
            suggestions, selectedSuggestion } = this.state;
        const { classes } = this.props;
        let { rows } = this.state;
        rows = (selectedSuggestion.length) > 0 ? selectedSuggestion : rowsData;
        const autosuggestProps = {
            renderInputComponent: this.renderInputComponent,
            suggestions: this.state.suggestions,
            onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
            onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
            getSuggestionValue: this.getSuggestionValue,
            renderSuggestion: this.renderSuggestion,
            onSuggestionSelected: this.onSuggestionSelected,
        };
        return (
            <div>
                <Paper className="customPaper" >
                    <GridMaterial
                        item
                        container
                        lg={12}
                        direction="row"
                        justify="center"
                    >
                        <Autosuggest
                            suggestions={suggestions}
                            {...autosuggestProps}
                            inputProps={{
                                classes,
                                placeholder: 'Search title',
                                value: this.state.single,
                                onChange: this.handleChange('single'),
                            }}
                            theme={{
                                container: classes.container,
                                suggestionsContainerOpen: classes.suggestionsContainerOpen,
                                suggestionsList: classes.suggestionsList,
                                suggestion: classes.suggestion,
                            }}
                            renderSuggestionsContainer={options => (
                                <Paper {...options.containerProps} square>
                                    {options.children}
                                </Paper>
                            )}
                        />
                    </GridMaterial>
                    {/* Grid to show csv data  */}
                    <Grid
                        rows={rows}
                        columns={columns}
                        xs={8}
                        direction="row"
                        justify="center"
                        className="dataGrid"
                    >
                        <DataTypeProvider
                            for={customColumns1}
                            formatterComponent={ImageLinkFormatter}
                        />
                        <TreeDataState />
                        <SelectionState />
                        <PagingState
                            defaultPageSize={pageSize} />
                        <IntegratedPaging />
                        <IntegratedSelection />
                        <RowDetailState
                            expandedRowIds={expandedRowIds}
                            onExpandedRowIdsChange={this.changeExpandedDetails}
                        />
                        <Table />
                        <TableColumnResizing defaultColumnWidths={defaultColumnWidths} />
                        <TableHeaderRow showSortingControls={false} className="tableHeaderRow" />
                        <TableRowDetail
                            contentComponent={RowDetail}
                            rowHeight={rowHeight}
                        />
                        <TableSelection showSelectAll={true} />
                        <PagingPanel
                            pageSizes={pageSizes} />
                    </Grid>
                </Paper>
            </div>
        )
    }
}

ReaderContainer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ReaderContainer);