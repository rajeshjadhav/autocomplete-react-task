import React, { Component } from 'react';
import './App.css';
import CSVreader from './containers/csv/';

class App extends Component {
  render() {
    return (
      <div className="App">       
        <CSVreader/>
      </div>
    );
  }
}

export default App;